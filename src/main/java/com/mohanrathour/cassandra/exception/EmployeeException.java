/**
 * 
 */
package com.mohanrathour.cassandra.exception;

/**
 * @author mohanrathour
 *
 */
public class EmployeeException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String errorMessage;
	
	/**
	 * 
	 */
	public EmployeeException() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param errorMessage
	 */
	public EmployeeException(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	
	

}
