package com.mohanrathour.cassandra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * 
 * @author mohanrathour
 *
 */
@SpringBootApplication
public class SpringBootCassandraApplication {

	/**
	 * Boot the SpringBootCassandraApplication 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(SpringBootCassandraApplication.class, args);
	}
}