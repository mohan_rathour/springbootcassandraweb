package com.mohanrathour.cassandra.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mohanrathour.cassandra.exception.EmployeeException;
import com.mohanrathour.cassandra.model.Employee;
import com.mohanrathour.cassandra.model.ErrorResponse;
import com.mohanrathour.cassandra.service.impl.EmployeeServiceImpl;

/**
 * Employee controller for handle all the employee rest api's calls
 * @author mohanrathour
 *
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class EmployeeController {
	
	@Autowired
	EmployeeServiceImpl employeeServiceImpl;
	
	/*@Autowired // no necessary in spring 4.3+
	public void employeeServiceImpl(EmployeeService employeeServiceImpl) {
	    this.employeeServiceImpl = (EmployeeServiceImpl) employeeServiceImpl;
	}*/

	/**
	 * This method will return the list of employee
	 * @return List<Employee>
	 */
	@GetMapping("/employees")
	public List<Employee> getAllEmployees() {
		System.out.println("Get all Employees...");
		return employeeServiceImpl.findAllEmplyees();
	}
	
	/**
	 * This method will handle the employee create functionality
	 * @param employee
	 * @return Employee
	 * @throws EmployeeException
	 */
	@PostMapping("/employees/create")
	public ResponseEntity<Employee> createEmployee(@Valid @RequestBody Employee employee) throws EmployeeException {
		System.out.println("Create Customer: " + employee.getName() + "...");
		return new ResponseEntity<>(employeeServiceImpl.createEmployee(employee), HttpStatus.OK);

	}
	
	/**
	 * This method will handle the update employee by employee id.
	 * @param id
	 * @param employee
	 * @return Employee
	 * @throws EmployeeException
	 */
	@PutMapping("/employees/{id}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable("id") UUID id, @RequestBody Employee employee) throws EmployeeException{
		System.out.println("Update Employee with ID = " + id + "...");
		return new ResponseEntity<>(employeeServiceImpl.updateEmployee(id, employee), HttpStatus.OK);
	}

	/**
	 * This method will handle the delete an employee by id.
	 * @param id
	 * @return String
	 * @throws EmployeeException 
	 */
	@DeleteMapping("/employees/{id}")
	public ResponseEntity<String> deleteCustomer(@PathVariable("id") UUID id) throws EmployeeException {
		System.out.println("Delete Customer with ID = " + id + "...");
		return new ResponseEntity<>(employeeServiceImpl.deleteEmployee(id), HttpStatus.OK);
	}

	/**
	 * This method will handle to delete all the employee.
	 * @return String
	 */
	@DeleteMapping("/employees/delete")
	public ResponseEntity<String> deleteAllemployees() {
		System.out.println("Delete All employees...");
		return new ResponseEntity<>(employeeServiceImpl.deleteAllemployees(), HttpStatus.OK);
	}

	/**
	 * This Method will handle the all the exceptions; 
	 * @param ex
	 * @return ErrorResponse
	 */
	@ExceptionHandler(EmployeeException.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(HttpStatus.NOT_FOUND.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
	}

}