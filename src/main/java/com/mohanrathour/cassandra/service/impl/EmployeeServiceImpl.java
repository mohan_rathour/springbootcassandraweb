/**
 * 
 */
package com.mohanrathour.cassandra.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.repository.support.BasicMapId;
import org.springframework.stereotype.Service;

import com.datastax.driver.core.utils.UUIDs;
import com.mohanrathour.cassandra.exception.EmployeeException;
import com.mohanrathour.cassandra.model.Employee;
import com.mohanrathour.cassandra.repository.EmployeeRepository;
import com.mohanrathour.cassandra.service.EmployeeService;

/**
 * @author mohanrathour
 *
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	EmployeeRepository employeeRepository;

	/*@Autowired // no necessary in spring 4.3+
	public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
	    this.employeeRepository = employeeRepository;
	}*/
	
	/**
	 * This method will return the list of employee
	 * @return List<Employee>
	 */
	@Override
	public List<Employee> findAllEmplyees() {
		List<Employee> employees = new ArrayList<>();
		employeeRepository.findAll().forEach(employees::add);
		return employees;
	}
	
	/**
	 * Create an employee
	 * @param employee
	 * @return Employee
	 * @throws EmployeeException
	 */
	@Override
	public Employee createEmployee(Employee employee) throws EmployeeException{
		employee.setId(UUIDs.timeBased());
		employee.setActive(false);
		return  employeeRepository.save(employee);
	}
	
	/**
	 * Update an employee
	 * @param id
	 * @param employee
	 * @return Employee
	 * @throws EmployeeException
	 */
	@Override
	public Employee updateEmployee(UUID id, Employee employee) throws EmployeeException {
		Employee employeeData = employeeRepository.findOne(BasicMapId.id("id", id));
		if (employeeData == null) {
			throw new EmployeeException("Invalid employee id requested");
		}
		employeeData.setName(employee.getName());
		employeeData.setAge(employee.getAge());
		employeeData.setEmail(employee.getEmail());
		employeeData.setMobileNumber(employee.getMobileNumber());
		employeeData.setActive(employee.isActive());
		return employeeRepository.save(employeeData);
	}
	
	/**
	 * Delete an Employee
	 * @param id
	 * @return String
	 * @throws EmployeeException
	 */
	@Override
	public String deleteEmployee(UUID id) throws EmployeeException {
		employeeRepository.delete(BasicMapId.id("id", id));
		return "Employee has been deleted!";
		
	}
	
	/**
	 * Delete all the employees
	 * @return String
	 */
	@Override
	public String deleteAllemployees(){
		employeeRepository.deleteAll();
		return "All employees have been deleted!";
	}
	
}
