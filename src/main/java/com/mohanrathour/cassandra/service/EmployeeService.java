/**
 * 
 */
package com.mohanrathour.cassandra.service;

import java.util.List;
import java.util.UUID;

import com.mohanrathour.cassandra.exception.EmployeeException;
import com.mohanrathour.cassandra.model.Employee;

/**
 * @author mohanrathour
 *
 */
public interface EmployeeService {
	
	/**
	 * Return the list of an employee
	 * @return List<Employee>
	 */
	public List<Employee> findAllEmplyees();
	
	/**
	 * Create an employee
	 * @param employee
	 * @return Employee
	 * @throws EmployeeException
	 */
	public Employee createEmployee(Employee employee) throws EmployeeException;
	/**
	 * Update an employee
	 * @param id
	 * @param employee
	 * @return Employee
	 * @throws EmployeeException
	 */
	public Employee updateEmployee(UUID id, Employee employee) throws EmployeeException;
	
	/**
	 * Delete an employee
	 * @param id
	 * @return String
	 * @throws EmployeeException
	 */
	public String deleteEmployee(UUID id) throws EmployeeException;
	
	/**
	 * Delete all the employees
	 * @return String
	 */
	public String  deleteAllemployees();
}
