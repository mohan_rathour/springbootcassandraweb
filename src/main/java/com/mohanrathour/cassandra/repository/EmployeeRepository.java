package com.mohanrathour.cassandra.repository;


import org.springframework.data.cassandra.repository.CassandraRepository;

import com.mohanrathour.cassandra.model.Employee;
/**
 * 
 * @author mohanrathour
 *
 */
public interface EmployeeRepository extends CassandraRepository<Employee> {
	//For by indexing you can use  
	
	/*@AllowFiltering
	public List<Employee> findByemail(String email);*/
}
